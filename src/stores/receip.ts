import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'

export const useReceipStore = defineStore('counter', () => {
    const authStore = useAuthStore()
    const memberStore = useMemberStore()
    const receiptDialog = ref(false)
    const receipt = ref<Receipt>({
        id: 0,
        createdDate: new Date(),
        totalBefor: 0,
        memberDiscount: 0,
        total: 0,
        receivedAmount: 0,
        change: 0,
        paymentTyoe: 'cash',
        employeeId: 0,
        userId: authStore.currantUser.id,
        user: authStore.currantUser,
        memberId: 0
    })
  const receiptItems = ref<ReceiptItem[]>([])
function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.products?.id === product.id)
    if (index >= 0) {
        receiptItems.value[index].unit++
        calReceipt()
        return
    } else {
        const newReceipt: ReceiptItem = {
            id: -1,
            name: product.name,
            price: product.price,
            unit: 1,
            productsId: product.id,
            products: product
        }
        receiptItems.value.push(newReceipt)
        calReceipt()
    }
}
function removeReciepItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
}
function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
}
function dec(item: ReceiptItem) {
    if (item.unit === 1) {
        removeReciepItem(item)
    } 
    item.unit--
    calReceipt()
    
}  

function calReceipt() {
    let totalBefor = 0
    for(const item of receiptItems.value){
        totalBefor = totalBefor + (item.price * item.unit)
    }
    receipt.value.totalBefor = totalBefor
    if(memberStore.currentMember){
        receipt.value.total = totalBefor * 0.95
    }else{
        receipt.value.total = totalBefor
    }
    
}
function showReceiptDialog(){
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true
}
function clear(){
    receiptItems.value =[]
    receipt.value = {
        id: 0,
        createdDate: new Date(),
        totalBefor: 0,
        memberDiscount: 0,
        total: 0,
        receivedAmount: 0,
        change: 0,
        paymentTyoe: 'cash',
        employeeId: 0,
        userId: authStore.currantUser.id,
        user: authStore.currantUser,
        memberId: 0
    }
    memberStore.clear()
}
  return {
    receiptItems, receipt, receiptDialog,
    addReceiptItem, removeReciepItem,inc,dec,calReceipt,showReceiptDialog,clear}
})
